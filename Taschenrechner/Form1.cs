﻿using System;
using System.Windows.Forms;

namespace Taschenrechner
{
 public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Methode btnAddition_Click wird definiert, wie die beiden Variablen (zahl1 und zahl2) 
        //beim klicken berechnet werden. Das Resultat dieser beiden Variablen wird in der Variable 
        //Ergebnis berechnet
        //Im Feld lblEreignis wird das Resultat ausgegeben
        private void btnAddition_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = zahl1 + zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "+";
        }
        //Methode btnSubtraktion_Click wird definiert, wie die beiden Variablen (zahl1 und zahl2) 
        //beim klicken berechnet werden. Das Resultat dieser beiden Variablen wird in der Variable 
        //Ergebnis berechnet
        //Im Feld lblEreignis wird das Resultat ausgegeben
        private void btnSubtraktion_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = zahl1 - zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "-";
        }
        //Methode btnMultiplikation_Click wird definiert, wie die beiden Variablen (zahl1 und zahl2) 
        //beim klicken berechnet werden. Das Resultat dieser beiden Variablen wird in der Variable 
        //Ergebnis berechnet
        //Im Feld lblEreignis wird das Resultat ausgegeben
        private void btnMultiplikation_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = zahl1 * zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "*";
        }
        
        //Methode btnDivision_Click wird definiert, wie die beiden Variablen (zahl1 und zahl2) 
        //beim klicken berechnet werden. Das Resultat dieser beiden Variablen wird in der Variable 
        //Ergebnis berechnet
        //Im Feld lblEreignis wird das Resultat ausgegeben
        private void btnDivision_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = zahl1 / zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "/";
        }
        //Methode btnMittelwert_Click wird definiert, wie die beiden Variablen (zahl1 und zahl2) 
        //beim klicken berechnet werden. Das Resultat dieser beiden Variablen wird in der Variable 
        //Ergebnis berechnet
        //Im Feld lblEreignis wird das Resultat ausgegeben
        private void btnMittelwert_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = (zahl1 + zahl2)/2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "Durchschnitt";
        }
        //Methode btnPotenz_Click wird definiert, wie die beiden Variablen (zahl1 und zahl2) 
        //beim klicken berechnet werden. Das Resultat dieser beiden Variablen wird in der Variable 
        //Ergebnis berechnet
        //Im Feld lblEreignis wird das Resultat ausgegeben
        private void btnPotenz_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = Math.Pow(zahl1, zahl2);
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "^";
        }
        //Methode btnMultiplikation_Click wird definiert, wie die beiden Variablen (zahl1 und zahl2) 
        //beim klicken berechnet werden. Das Resultat dieser beiden Variablen wird in der Variable 
        //Es gibt bei Max keine Formel - Möglichkeit auf Math.Max zu zugreifen.
        //Ergebnis in lblErgebnis
        private void btnMaximum_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = Math.Max(zahl1, zahl2);
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "Max";
        }
        //Methode btnMultiplikation_Click wird definiert, wie die beiden Variablen (zahl1 und zahl2) 
        //beim klicken berechnet werden. Das Resultat dieser beiden Variablen wird in der Variable 
        //Es gibt bei Min keine Formel - Möglichkeit auf Math.Min zu zugreifen.
        //Ergebnis in lblErgebnis
        private void btnMinimum_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = Math.Min(zahl1, zahl2);
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "Max";
        }
        //Methode btnPotenz_Click wird definiert, wie die beiden Variablen (zahl1 und zahl2) 
        //beim klicken berechnet werden. Das Resultat dieser beiden Variablen wird in der Variable 
        //Ergebnis berechnet
        //Im Feld lblEreignis wird das Resultat ausgegeben
        private void btnModulo_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = zahl1 % zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "Modulo";
        }
    }
    }