﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Schachbrett
{
    //Variable x und y beziehen sich auf die horizontale resp vertikale
    //X und y Counter zeigt die Verschiebung der Felder
    //Variable isBlack beschreibt dessen Farbe
    
    public partial class Form1 : Form
    {
        private int x = 0;
        private int y = 0;

        private int xCounter = 0;
        private int yCounter = 0;

        private bool isBlack = true;

        public Form1()
        {
            InitializeComponent();
        }
        //Methode btnSpielStart_Click startet das Schachbrett mit Clear wird alles gelöscht und mit Zeichneschachbrett 
        //gezeichnet
        private void btnSpielStart_Click(object sender, EventArgs e)
        {
            clear();
            ZeichneSchachbrett();
        }
        //Methode ZeichneSchachbrett beginnt das Zeichnen über die Variablen Isblack sowie deren Abtausch mit Color.White
        //
        private void ZeichneSchachbrett()
        {
            int size = (int) cmpDropDown.Value;

            while (yCounter < size)
            {
                Label label = new Label();

                if (isBlack == true)
                {
                    label.BackColor = Color.Black;
                }
                else
                {
                    label.BackColor = Color.White;
                }

                isBlack = !isBlack;


                label.Size = new Size(20, 20);
                label.Location = new Point(x, y);


                pnlSpielfeld.Controls.Add(label);

                x += 20;
                xCounter++;

                if (xCounter == size)
                {
                    x = 0;
                    y += 20;
                    xCounter = 0;
                    yCounter++;

                    if (size % 2 == 0)
                    {
                        isBlack = !isBlack;
                    }
                }
            }
        }

        //spielfeld wird gelöscht
        private void clear()
        {
            x = 0;
            y = 0;
            
            xCounter = 0;
            yCounter = 0;

            isBlack = true;

            pnlSpielfeld.Controls.Clear();

        }

    }
}