﻿namespace Schachbrett
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSpielfeld = new System.Windows.Forms.Panel();
            this.cmpDropDown = new System.Windows.Forms.NumericUpDown();
            this.btnSpielStart = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize) (this.cmpDropDown)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSpielfeld
            // 
            this.pnlSpielfeld.Location = new System.Drawing.Point(63, 206);
            this.pnlSpielfeld.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlSpielfeld.Name = "pnlSpielfeld";
            this.pnlSpielfeld.Size = new System.Drawing.Size(512, 483);
            this.pnlSpielfeld.TabIndex = 0;
            // 
            // cmpDropDown
            // 
            this.cmpDropDown.Location = new System.Drawing.Point(317, 77);
            this.cmpDropDown.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmpDropDown.Name = "cmpDropDown";
            this.cmpDropDown.Size = new System.Drawing.Size(168, 31);
            this.cmpDropDown.TabIndex = 1;
            // 
            // btnSpielStart
            // 
            this.btnSpielStart.Location = new System.Drawing.Point(67, 73);
            this.btnSpielStart.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSpielStart.Name = "btnSpielStart";
            this.btnSpielStart.Size = new System.Drawing.Size(235, 52);
            this.btnSpielStart.TabIndex = 2;
            this.btnSpielStart.Text = "Spiel starten";
            this.btnSpielStart.UseVisualStyleBackColor = true;
            this.btnSpielStart.Click += new System.EventHandler(this.btnSpielStart_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1333, 865);
            this.Controls.Add(this.btnSpielStart);
            this.Controls.Add(this.cmpDropDown);
            this.Controls.Add(this.pnlSpielfeld);
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize) (this.cmpDropDown)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlSpielfeld;
        private System.Windows.Forms.NumericUpDown cmpDropDown;
        private System.Windows.Forms.Button btnSpielStart;
    }
}