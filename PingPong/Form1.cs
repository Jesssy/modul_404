﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PingPong
{
    //Variable x und y Achse / Bewegung / Verschiebt sich um genannte Pixel
    public partial class Form1 : Form
    {
        private int bewegungX = 5;
        private int bewegungY = 2;


        public Form1()
        {
            InitializeComponent();
        }
        //Methode btnStart_Click startes das Spiel
        private void btnStart_Click(object sender, EventArgs e)
        {
            tmrSpiel.Start();
        }
        //MEthode tmrSpiel_Tick_1 startet den Ball und definiert dessen Richtung und Richtungswechsel
        private void tmrSpiel_Tick_1(object sender, EventArgs e)
        {
            Point location = picBall.Location;
            location.X += bewegungX;
            location.Y += bewegungY;
            picBall.Location = location;


            Size spielfeld = pnlSpiel.Size;
            if (location.Y + picBall.Size.Height > spielfeld.Height || location.Y < 0)
            {
                bewegungY *= -1;
            }

            if (location.X + picBall.Size.Width > spielfeld.Width || location.X < 0)
            {
                bewegungX *= -1;

                if (picBall.Location.Y >= picSchlägerRe.Location.Y &&
                    picBall.Location.Y <= picSchlägerRe.Location.Y + picSchlägerRe.Size.Height)
                {
                }
            }
        }
        //Methode vScrollbar2_Scroll definiert das Zusammenspiel von Scrollbar und Schläger
        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            int maxSchlaegerPosition = pnlSpiel.Size.Height - picSchlägerRe.Size.Height;
            int positionProzent = vsbScrollRe.Value;
            int position = maxSchlaegerPosition * positionProzent / 100;

            Point schlaegerPosition = picSchlägerRe.Location;
            schlaegerPosition.Y = position;
            picSchlägerRe.Location = schlaegerPosition;
        }
        //Methode damit die weiteren Eingaben geschehen können auch wenn der Cursor nicht im Spielfeld ist
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        //Hier wird beschrieben was passiert wenn man die genannten Tasten drückt / Welche Bewegungen geschehen
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.H:
                    bewegungX *= -1;
                    break;

                case Keys.V:
                    bewegungY *= -1;
                    break;

                case Keys.P:
                    tmrSpiel.Stop();
                    break;

                case Keys.S:
                    tmrSpiel.Start();
                    break;
            }


            return base.ProcessCmdKey(ref msg, keyData);
        }
        
        //Methode die Angibt wie viel sich der Ball veschiebt Richtungswechel mit den Pfeiltasten
        private void versetzeBall(String richtung)
        {
            int x = 0;
            int y = 0;

            switch (richtung)
            {
                case "ho":
                    y -= 25;
                    break;

                case "ru":
                    y += 25;
                    break;

                case "li":
                    x -= 25;
                    break;

                case "re":
                    x += 25;
                    break;
            }

            Point ballPosition = picBall.Location;
            ballPosition.Y += y;
            ballPosition.X += x;
            picBall.Location = ballPosition;
        }

        //Methode die den Ball bei Klick der Pfeiltasten verschiebt
        private void btnLinks_Click(object sender, EventArgs e)
        {
            versetzeBall("li");
        }

        private void btnRunter_Click_1(object sender, EventArgs e)
        {
            versetzeBall("ru");
        }

        private void btnRechts_Click_1(object sender, EventArgs e)
        {
            versetzeBall("re");
        }

        private void btnHoch_Click(object sender, EventArgs e)
        {
            versetzeBall("re");
        }
    }
}