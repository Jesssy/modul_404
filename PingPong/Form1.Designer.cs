﻿namespace PingPong
{
    partial class Form1
    {
               /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pnlSpiel = new System.Windows.Forms.Panel();
            this.picSchlägerRe = new System.Windows.Forms.PictureBox();
            this.picBall = new System.Windows.Forms.PictureBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.vsbScrollRe = new System.Windows.Forms.VScrollBar();
            this.lblPunkte = new System.Windows.Forms.Label();
            this.txtPunkte = new System.Windows.Forms.TextBox();
            this.btnLinks = new System.Windows.Forms.Button();
            this.btnRunter = new System.Windows.Forms.Button();
            this.tmrSpiel = new System.Windows.Forms.Timer(this.components);
            this.lblText = new System.Windows.Forms.Label();
            this.btnRechts = new System.Windows.Forms.Button();
            this.btnHoch = new System.Windows.Forms.Button();
            this.pnlSpiel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.picSchlägerRe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.picBall)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSpiel
            // 
            this.pnlSpiel.BackColor = System.Drawing.Color.SeaGreen;
            this.pnlSpiel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSpiel.Controls.Add(this.picSchlägerRe);
            this.pnlSpiel.Controls.Add(this.picBall);
            this.pnlSpiel.Location = new System.Drawing.Point(24, 28);
            this.pnlSpiel.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.pnlSpiel.Name = "pnlSpiel";
            this.pnlSpiel.Size = new System.Drawing.Size(539, 427);
            this.pnlSpiel.TabIndex = 0;
            // 
            // picSchlägerRe
            // 
            this.picSchlägerRe.BackColor = System.Drawing.Color.Black;
            this.picSchlägerRe.Location = new System.Drawing.Point(523, 94);
            this.picSchlägerRe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picSchlägerRe.Name = "picSchlägerRe";
            this.picSchlägerRe.Size = new System.Drawing.Size(3, 40);
            this.picSchlägerRe.TabIndex = 1;
            this.picSchlägerRe.TabStop = false;
            // 
            // picBall
            // 
            this.picBall.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.picBall.Location = new System.Drawing.Point(163, 178);
            this.picBall.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.picBall.Name = "picBall";
            this.picBall.Size = new System.Drawing.Size(34, 42);
            this.picBall.TabIndex = 0;
            this.picBall.TabStop = false;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(24, 635);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(144, 62);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Spiel starten";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // vsbScrollRe
            // 
            this.vsbScrollRe.Location = new System.Drawing.Point(568, 28);
            this.vsbScrollRe.Name = "vsbScrollRe";
            this.vsbScrollRe.Size = new System.Drawing.Size(30, 425);
            this.vsbScrollRe.TabIndex = 2;
            this.vsbScrollRe.Value = 50;
            this.vsbScrollRe.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar1_Scroll);
            // 
            // lblPunkte
            // 
            this.lblPunkte.Location = new System.Drawing.Point(23, 494);
            this.lblPunkte.Name = "lblPunkte";
            this.lblPunkte.Size = new System.Drawing.Size(147, 62);
            this.lblPunkte.TabIndex = 3;
            this.lblPunkte.Text = "Punkte:";
            // 
            // txtPunkte
            // 
            this.txtPunkte.Location = new System.Drawing.Point(137, 494);
            this.txtPunkte.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPunkte.Name = "txtPunkte";
            this.txtPunkte.Size = new System.Drawing.Size(112, 31);
            this.txtPunkte.TabIndex = 4;
            this.txtPunkte.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnLinks
            // 
            this.btnLinks.BackgroundImage = ((System.Drawing.Image) (resources.GetObject("btnLinks.BackgroundImage")));
            this.btnLinks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnLinks.Location = new System.Drawing.Point(692, 222);
            this.btnLinks.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLinks.Name = "btnLinks";
            this.btnLinks.Size = new System.Drawing.Size(40, 38);
            this.btnLinks.TabIndex = 6;
            this.btnLinks.UseVisualStyleBackColor = true;
            this.btnLinks.Click += new System.EventHandler(this.btnLinks_Click);
            // 
            // btnRunter
            // 
            this.btnRunter.BackgroundImage =
                ((System.Drawing.Image) (resources.GetObject("btnRunter.BackgroundImage")));
            this.btnRunter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRunter.Location = new System.Drawing.Point(762, 292);
            this.btnRunter.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRunter.Name = "btnRunter";
            this.btnRunter.Size = new System.Drawing.Size(40, 38);
            this.btnRunter.TabIndex = 7;
            this.btnRunter.UseVisualStyleBackColor = true;
            this.btnRunter.Click += new System.EventHandler(this.btnRunter_Click_1);
            // 
            // tmrSpiel
            // 
            this.tmrSpiel.Interval = 120;
            this.tmrSpiel.Tick += new System.EventHandler(this.tmrSpiel_Tick_1);
            // 
            // lblText
            // 
            this.lblText.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblText.Location = new System.Drawing.Point(288, 546);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(308, 175);
            this.lblText.TabIndex = 9;
            this.lblText.Text =
                "Tastatursteuerung\r\nTaste:\r\nH  horiziontale Flugrichtung ändern\r\nV  vertikale Flug" +
                "richtung ändern\r\nP  Pause\r\nS  Spiel weiterlafuen lassen";
            // 
            // btnRechts
            // 
            this.btnRechts.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRechts.BackgroundImage =
                ((System.Drawing.Image) (resources.GetObject("btnRechts.BackgroundImage")));
            this.btnRechts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRechts.Location = new System.Drawing.Point(843, 222);
            this.btnRechts.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRechts.Name = "btnRechts";
            this.btnRechts.Size = new System.Drawing.Size(40, 38);
            this.btnRechts.TabIndex = 10;
            this.btnRechts.UseVisualStyleBackColor = false;
            this.btnRechts.Click += new System.EventHandler(this.btnRechts_Click_1);
            // 
            // btnHoch
            // 
            this.btnHoch.BackgroundImage = ((System.Drawing.Image) (resources.GetObject("btnHoch.BackgroundImage")));
            this.btnHoch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnHoch.Location = new System.Drawing.Point(762, 156);
            this.btnHoch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnHoch.Name = "btnHoch";
            this.btnHoch.Size = new System.Drawing.Size(40, 38);
            this.btnHoch.TabIndex = 11;
            this.btnHoch.UseVisualStyleBackColor = true;
            this.btnHoch.Click += new System.EventHandler(this.btnHoch_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 772);
            this.Controls.Add(this.btnHoch);
            this.Controls.Add(this.btnRechts);
            this.Controls.Add(this.lblText);
            this.Controls.Add(this.btnRunter);
            this.Controls.Add(this.btnLinks);
            this.Controls.Add(this.txtPunkte);
            this.Controls.Add(this.lblPunkte);
            this.Controls.Add(this.vsbScrollRe);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.pnlSpiel);
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "Form1";
            this.Text = "Ping-Pong Spiel";
            this.Click += new System.EventHandler(this.Form1_Load);
            this.pnlSpiel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.picSchlägerRe)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.picBall)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Panel pnlSpiel;
        private System.Windows.Forms.PictureBox picBall;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.VScrollBar vsbScrollRe;
        private System.Windows.Forms.PictureBox picSchlägerRe;
        private System.Windows.Forms.Label lblPunkte;
        private System.Windows.Forms.TextBox txtPunkte;
        private System.Windows.Forms.Button btnHoch;
        private System.Windows.Forms.Button btnLinks;
        private System.Windows.Forms.Button btnRunter;
        private System.Windows.Forms.Timer tmrSpiel;
        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.Button btnRechts;
    }
}